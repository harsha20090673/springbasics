package com.harsha.SpringBasics;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class MovieClient {

	public static void main(String[] args) {
		ApplicationContext context= new ClassPathXmlApplicationContext("SpringBasics.xml");
		Movie movie=(Movie)context.getBean("basicspringbean");
		System.out.println(movie.getMovieId());
		System.out.println(movie.getMovieName());
		System.out.println(movie.getMovieActor());
	}

}
